package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

// # objects and arrays
type ordersItem struct {
	ORDERKEY    int
	CUSTKEY     int
	ORDERSTATUS string
	TOTALPRICE  int
	ORDERDATE   time.Time
}
type orders struct {
	ordersItems []*ordersItem
}
type customerItem struct {
	CUSTKEY    int
	Name       string
	MKTSEGMENT string
	COMMENT    string
}
type customer struct {
	customerItems []*customerItem
}
type lineItem struct {
	ORDERKEY      int
	PARTKEY       int
	SUPPKEY       int
	EXTENDEDPRICE float64
	DISCOUNT      float64
}
type lineItemList struct {
	lineItems []*lineItem
}
type revenue struct {
	rev float64
}
type revenueList struct {
	revenues []*revenue
}

// filepath directories
var (
	ordersPath, _    = filepath.Abs("./datasets/dateset_1/orders.tbl/orders.tbl")
	customersPath, _ = filepath.Abs("./datasets/dateset_1/customer.tbl/customer.tbl")
	lineItemsPath, _ = filepath.Abs("./datasets/dateset_1/lineitem.tbl/lineitem.tbl")
)

func newOrder(ORDERKEY int, CUSTKEY int, ORDERSTATUS string, TOTALPRICE int, ORDERDATE time.Time) *ordersItem {
	o := ordersItem{ORDERKEY: ORDERKEY, CUSTKEY: CUSTKEY, ORDERSTATUS: ORDERSTATUS, TOTALPRICE: TOTALPRICE, ORDERDATE: ORDERDATE}
	return &o
}
func newCustomer(CUSTKEY int, Name string, MKTSEGMENT string, COMMENT string) *customerItem {
	c := customerItem{CUSTKEY: CUSTKEY, Name: Name, MKTSEGMENT: MKTSEGMENT, COMMENT: COMMENT}
	return &c
}
func newLineItem(ORDERKEY int, PARTKEY int, SUPPKEY int, EXTENDEDPRICE float64, DISCOUNT float64) *lineItem {
	l := lineItem{ORDERKEY: ORDERKEY, PARTKEY: PARTKEY, SUPPKEY: SUPPKEY, EXTENDEDPRICE: EXTENDEDPRICE, DISCOUNT: DISCOUNT}
	return &l
}

func main() {

	filteredlistItems := make([]*lineItem, 0)
	_revenue := make([]*revenue, 0)

	// return values from files
	ordl := getOrderList()
	cstl := getCustomerList()
	li := getLineItems()

	filteredCustomerItems := filterCustomer(cstl, "AUTOMOBILE")

	//  search for CUSTOMERKEYS
	for _, each_fltrd := range filteredCustomerItems {
		filteredOrderByCustomer := filterOrderByCustomer(ordl, each_fltrd.CUSTKEY)

		// search for ORDERKEY
		for _, each_line := range filteredOrderByCustomer {
			_filteredlistItems := filterLineItemByOrderKey(li, each_line.ORDERKEY)

			// calculate revenues
			for _, each_lineIt := range _filteredlistItems {
				filteredlistItems = append(filteredlistItems, each_lineIt)
				r := each_lineIt.EXTENDEDPRICE * (1 - each_lineIt.DISCOUNT)
				_revenue = append(_revenue, &revenue{rev: r})
				fmt.Printf("Revenue : %v \n", r)
			}
		}
	}

}

// search for MKTSEGMENT
func filterCustomer(_customerItem []*customerItem, text string) (temp []*customerItem) {

	for _, each_cust := range _customerItem {
		if each_cust.MKTSEGMENT == text {
			temp = append(temp, each_cust)
		}
	}
	return temp
}

// search for CUSTOMERKEYS
func filterOrderByCustomer(_orderItem []*ordersItem, custkey int) (temp []*ordersItem) {

	for _, each_ord := range _orderItem {
		if each_ord.CUSTKEY == custkey {
			temp = append(temp, each_ord)
		}
	}
	return temp
}

// search for ORDERKEYS
func filterLineItemByOrderKey(_lineItem []*lineItem, orderkey int) (temp []*lineItem) {

	for _, each_lineIt := range _lineItem {
		if each_lineIt.ORDERKEY == orderkey {
			temp = append(temp, each_lineIt)
		}
	}
	return temp
}

// read file from orders.tbl
func getOrderList() (ordersItems []*ordersItem) {

	// os.Open() opens specific file in
	// read-only mode and this return
	// a pointer of type os.

	file, err := os.Open(ordersPath)
	if err != nil {
		log.Fatalf("failed to open")

	}

	// The bufio.NewScanner() function is called in which the
	// object os.File passed as its parameter and this returns a
	// object bufio.Scanner which is further used on the
	// bufio.Scanner.Split() method.
	scanner := bufio.NewScanner(file)

	// The bufio.ScanLines is used as an
	// input to the method bufio.Scanner.Split()
	// and then the scanning forwards to each
	// new line using the bufio.Scanner.Scan()
	// method.
	scanner.Split(bufio.ScanLines)
	var text []string

	for scanner.Scan() {
		text = append(text, scanner.Text())
	}

	// The method os.File.Close() is called
	// on the os.File object to close the file
	file.Close()

	// and then a loop iterates through
	// and prints each of the slice values.
	orderItems := make([]*ordersItem, 0)
	for _, each_ln := range text {
		// # split each line into a slice of values line by line
		// # and then print the values
		db := strings.Split(each_ln, "|")
		// # print the values
		orderKey, _ := strconv.Atoi(db[0])
		custKey, _ := strconv.Atoi(db[1])
		orderStatus := db[2]
		totalPrice, _ := strconv.Atoi(db[3])
		orderDate, _ := time.Parse("2006-01-02", db[4])

		x := newOrder(orderKey, custKey, orderStatus, totalPrice, orderDate)

		orderItems = append(orderItems, x)
	}
	return orderItems
}

// read file from customer.tbl
func getCustomerList() (customerItems []*customerItem) {

	// os.Open() opens specific file in
	// read-only mode and this return
	// a pointer of type os.
	file, err := os.Open(customersPath)

	if err != nil {
		log.Fatalf("failed to open")

	}

	// The bufio.NewScanner() function is called in which the
	// object os.File passed as its parameter and this returns a
	// object bufio.Scanner which is further used on the
	// bufio.Scanner.Split() method.
	scanner := bufio.NewScanner(file)

	// The bufio.ScanLines is used as an
	// input to the method bufio.Scanner.Split()
	// and then the scanning forwards to each
	// new line using the bufio.Scanner.Scan()
	// method.
	scanner.Split(bufio.ScanLines)
	var text []string

	for scanner.Scan() {
		text = append(text, scanner.Text())
	}

	// The method os.File.Close() is called
	// on the os.File object to close the file
	file.Close()

	// and then a loop iterates through
	// and prints each of the slice values.
	customerItems = make([]*customerItem, 0)

	for _, each_ln := range text {
		// # split each line into a slice of values line by line
		// # and then print the values
		db := strings.Split(each_ln, "|")
		// # print the values
		custKey, _ := strconv.Atoi(db[0])
		name := db[1]
		mktSegment := db[6]
		comment := db[7]

		x := newCustomer(custKey, name, mktSegment, comment)
		customerItems = append(customerItems, x)

	}
	return customerItems
}

// read file from lineitem.tbl
func getLineItems() (lineItemsList []*lineItem) {

	// os.Open() opens specific file in
	// read-only mode and this return
	// a pointer of type os.
	file, err := os.Open(lineItemsPath)

	if err != nil {
		log.Fatalf("failed to open")

	}

	// The bufio.NewScanner() function is called in which the
	// object os.File passed as its parameter and this returns a
	// object bufio.Scanner which is further used on the
	// bufio.Scanner.Split() method.
	scanner := bufio.NewScanner(file)

	// The bufio.ScanLines is used as an
	// input to the method bufio.Scanner.Split()
	// and then the scanning forwards to each
	// new line using the bufio.Scanner.Scan()
	// method.
	scanner.Split(bufio.ScanLines)
	var text []string

	for scanner.Scan() {
		text = append(text, scanner.Text())
	}

	// The method os.File.Close() is called
	// on the os.File object to close the file
	file.Close()

	// and then a loop iterates through
	// and prints each of the slice values.

	for _, each_ln := range text {
		// # split each line into a slice of values line by line
		// # and then print the values
		db := strings.Split(each_ln, "|")
		// # print the values
		orderKey, _ := strconv.Atoi(db[0])
		partKey, _ := strconv.Atoi(db[1])
		suppKey, _ := strconv.Atoi(db[2])
		extendedPrice, _ := strconv.ParseFloat(db[5], 32)
		discount, _ := strconv.ParseFloat(db[6], 32)
		x := newLineItem(orderKey, partKey, suppKey, extendedPrice, discount)
		lineItemsList = append(lineItemsList, x)

	}
	return lineItemsList
}
